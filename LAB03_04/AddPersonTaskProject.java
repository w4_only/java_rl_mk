import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import apps.people.Member;
import apps.projects.Task;



public class AddPersonTaskProject extends JPanel
{
	private JTextField title;
    private JTextField date;
    
    private JButton addButton;
    private JButton cancelButton;
    private boolean ok;
    private JDialog dialog;
    private int selectedIndexTask;
    private int selectedIndexPerson;
    
    public AddPersonTaskProject(ArrayList<Task> tasks, ArrayList<Member> persons) {
    	setLayout(new BorderLayout());
    	JPanel panel = new JPanel();
    	panel.setLayout(new GridLayout(3, 2));
    	panel.add(new JLabel("Select task:"));
    	JComboBox comboBoxTask = new JComboBox(tasks.toArray());
    	panel.add(comboBoxTask);
    	selectedIndexTask = comboBoxTask.getSelectedIndex();
    	panel.add(new JLabel("Select person:"));
    	JComboBox comboBoxPerson = new JComboBox(persons.toArray());
    	panel.add(comboBoxPerson);
    	selectedIndexPerson = comboBoxPerson.getSelectedIndex();
    	add(panel, BorderLayout.CENTER);
    	
    	addButton = new JButton("Add person to task");
    	addButton.addActionListener(event -> {
    		
    		ok = true;
    		dialog.setVisible(false);
    	});
    	
    	cancelButton = new JButton("Cancel");
    	cancelButton.addActionListener(event -> dialog.setVisible(false));
    	
    	JPanel buttonPanel = new JPanel();
    	buttonPanel.add(addButton);
    	buttonPanel.add(cancelButton);
    	add(buttonPanel, BorderLayout.SOUTH);
    }
    
    
	public int getIndexTask()
	{
		return selectedIndexTask;
	}
	
	public int getIndexPerson()
	{
		return selectedIndexPerson;
	}
    
    public boolean showDialog(Component parent, String title) {
    	ok = false;
    	
    	Frame owner = null;
    	if(parent instanceof Frame)
    		owner = (Frame) parent;
    	else
    		owner = (Frame) SwingUtilities.getAncestorOfClass(Frame.class, parent);
    	
    	if(dialog == null || dialog.getOwner() != owner)
    	{
    		dialog = new JDialog(owner, true);
    		dialog.add(this);
    		dialog.getRootPane().setDefaultButton(addButton);
    		dialog.pack();
    	}
    	
    	dialog.setTitle(title);
    	dialog.setVisible(true);
    	return ok;
    }
	

}
