import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import apps.people.Member;
import apps.projects.Project;
import apps.projects.Task;



public class ModifyProject extends JPanel
{
	Project p;
	private JTextField title;
    
    private JButton modifyButton;
    private JButton cancelButton;
    private boolean ok;
    private JDialog dialog;
    
    public ModifyProject(Project p) {
    	this.p = p;
    	setLayout(new BorderLayout());
       	JPanel panel = new JPanel();
    	panel.setLayout(new GridLayout(1, 2));
    	panel.add(new JLabel("Title:"));
    	panel.add(title = new JTextField(p.getTitle()));
    	add(panel, BorderLayout.CENTER);
    	
    	modifyButton = new JButton("Modify");
    	modifyButton.addActionListener(event -> {
    		ok = true;
    		dialog.setVisible(false);
    	});
    	
    	cancelButton = new JButton("Cancel");
    	cancelButton.addActionListener(event -> dialog.setVisible(false));
    	
    	JPanel buttonPanel = new JPanel();
    	buttonPanel.add(modifyButton);
    	buttonPanel.add(cancelButton);
    	add(buttonPanel, BorderLayout.SOUTH);
    }
    
    public Project getProject() {
    	return new Project(title.getText()
    					);
    }
    
    
    public boolean showDialog(Component parent, String title) {
    	ok = false;
    	
    	Frame owner = null;
    	if(parent instanceof Frame)
    		owner = (Frame) parent;
    	else
    		owner = (Frame) SwingUtilities.getAncestorOfClass(Frame.class, parent);
    	
    	if(dialog == null || dialog.getOwner() != owner)
    	{
    		dialog = new JDialog(owner, true);
    		dialog.add(this);
    		dialog.getRootPane().setDefaultButton(modifyButton);
    		dialog.pack();
    	}
    	
    	dialog.setTitle(title);
    	dialog.setVisible(true);
    	return ok;
    }
	

}
