import apps.projects.Task;
import javafx.scene.control.ComboBox;
import apps.projects.Project;
import apps.people.Member;

import java.util.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.*;
import java.awt.*;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;




class ProjectManger
{
	
	
    public static void main(String[] args) throws MyFileException
    {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				
				ProjectManagerFrame frame = new ProjectManagerFrame();
		        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		        frame.setTitle("ProjectManager");
		        frame.setVisible(true);
			}
		});
    }
}


/**************************************************************************************/


class ProjectManagerFrame extends JFrame {

	private static final int DEFAULT_WIDTH = 900;
	private static final int DEFAULT_HEIGHT = 700;
	
	private AddPerson addPerson = null;
	private AddTask addTask = null;
	private AddProject addProject = null;
	private ModifyPerson modifyPerson = null;
	private ModifyTask modifyTask = null;
	private ModifyProject modifyProject = null;
	private ModifyPersonProject modifyPersonProject = null;
	private DeletePersonProject deletePersonProject = null;
	private ModifyTaskProject modifyTaskProject = null;
	private DeleteTaskProject deleteTaskProject = null;
	private AddPersonTaskProject addPersonTaskProject = null;
	private DeletePeronTaskPeroject deletePersonTaskProject = null;
	
	private PersonTableModel modelPerson;
	private JTable tablePerson;
	private TaskTableModel modelTask;
	private JTable tableTask;
	private ProjectTableModel modelProject;
	private JTable tableProject;
	
	private JFileChooser inFile;
	private JFileChooser outFile;
	
	private JTabbedPane tabbedPane;
	

	public ProjectManagerFrame() {
		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

		/*Pasek menu*/
		JMenuBar menuBar = new JMenuBar();
		
		JMenu personMenu = new JMenu("Person");
		JMenuItem addPerson = new JMenuItem("Add person");
		addPerson.addActionListener(new NewPersonAction());
		personMenu.add(addPerson);
		JMenuItem deletePerson = new JMenuItem("Delete person");
		deletePerson.addActionListener(new DeletePersonAction());
		personMenu.add(deletePerson);
		JMenuItem modifyPerson = new JMenuItem("Modify person");
		modifyPerson.addActionListener(new ModifyPersonAction());
		personMenu.add(modifyPerson);
		menuBar.add(personMenu);
		
		JMenu taskMenu = new JMenu("Task");
		JMenuItem addTask = new JMenuItem("Add task");
		addTask.addActionListener(new NewTaskAction());
		taskMenu.add(addTask);
		JMenuItem deleteTask = new JMenuItem("Delete task");
		deleteTask.addActionListener(new DeleteTaskAction());
		taskMenu.add(deleteTask);
		JMenuItem modifyTask = new JMenuItem("Modify task");
		modifyTask.addActionListener(new ModifyTaskAction());
		taskMenu.add(modifyTask);
		taskMenu.addSeparator();
		JMenuItem addPersonTask = new JMenuItem("Add person to task");
		addPersonTask.addActionListener(new AddPersonTaskAction());
		taskMenu.add(addPersonTask);
		JMenuItem deletePersonTask = new JMenuItem("Delete person from task");
		deletePersonTask.addActionListener(new DeletePersonTaskAction());
		taskMenu.add(deletePersonTask);
		JMenuItem modifyPersonTask = new JMenuItem("Modify person in task");
		modifyPersonTask.addActionListener(new ModifyPersonTaskAction());
		taskMenu.add(modifyPersonTask);
		taskMenu.addSeparator();
		JMenuItem sortTaskAsc = new JMenuItem("Sort asc");
		sortTaskAsc.addActionListener(new SortTaskAscAction());
		taskMenu.add(sortTaskAsc);
		JMenuItem sortTaskDesc = new JMenuItem("Sort desc");
		sortTaskDesc.addActionListener(new SortTaskDescAction());
		taskMenu.add(sortTaskDesc);
		menuBar.add(taskMenu);
		
		JMenu projectMenu = new JMenu("Project");
		JMenuItem addProject = new JMenuItem("Add project");
		addProject.addActionListener(new NewProjectAction());
		projectMenu.add(addProject);
		JMenuItem deleteProject = new JMenuItem("Delete project");
		deleteProject.addActionListener(new DeleteProjectAction());
		projectMenu.add(deleteProject);
		JMenuItem modifyProject = new JMenuItem("Modify project");
		//modifyProject.addActionListener(new ModifyProjectAction());
		projectMenu.add(modifyProject);
		projectMenu.addSeparator();
		JMenuItem addPerosnProject = new JMenuItem("Add person to project");
		addPerosnProject.addActionListener(new AddPersonProjectAction());
		projectMenu.add(addPerosnProject);
		JMenuItem deletePerosnProject = new JMenuItem("Delete person from project");
		deletePerosnProject.addActionListener(new DeletePersonProjectAction());
		projectMenu.add(deletePerosnProject);
		JMenuItem modifyPerosnProject = new JMenuItem("Modify person in project");
		modifyPerosnProject.addActionListener(new ModifyPersonProjectAction());
		projectMenu.add(modifyPerosnProject);
		projectMenu.addSeparator();
		JMenuItem addTaskProject = new JMenuItem("Add task to project");
		addTaskProject.addActionListener(new AddTaskProjectAction());
		projectMenu.add(addTaskProject);
		JMenuItem deleteTaskProject = new JMenuItem("Delete task from project");
		deleteTaskProject.addActionListener(new DeleteTaskProjectAction());
		projectMenu.add(deleteTaskProject);
		JMenuItem modifyTaskProject = new JMenuItem("Modify task in project");
		modifyTaskProject.addActionListener(new ModifyTaskProjectAction());
		projectMenu.add(modifyTaskProject);
		projectMenu.addSeparator();
		JMenuItem addPersonTaskProject = new JMenuItem("Add person to task in project");
		addPersonTaskProject.addActionListener(new AddPersonTaskProjectAction());
		projectMenu.add(addPersonTaskProject);
		JMenuItem deletePersonTaskProject = new JMenuItem("Delete person form task in project");
		deletePersonTaskProject.addActionListener(new DeletePersonTaskProjectAction());
		projectMenu.add(deletePersonTaskProject);
		projectMenu.addSeparator();
		JMenuItem sortTasProjectkAsc = new JMenuItem("Sort task asc");
		sortTasProjectkAsc.addActionListener(new SortTaskProjectAscAction());
		projectMenu.add(sortTasProjectkAsc);
		JMenuItem sortTaskProjectDesc = new JMenuItem("Sort task desc");
		sortTaskProjectDesc.addActionListener(new SortTaskProjectDescAction());
		projectMenu.add(sortTaskProjectDesc);
		menuBar.add(projectMenu);
		
		JMenu impexpMenu = new JMenu("Export/Import"); 
		FileNameExtensionFilter filter = new FileNameExtensionFilter("only text file...", "txt", "text");
		inFile = new JFileChooser();
		inFile.setFileFilter(filter);
		outFile = new JFileChooser();
		outFile.setFileFilter(filter);
		JMenuItem importProject = new JMenuItem("Import <-");
		importProject.addActionListener(event -> {
			inFile.setCurrentDirectory(new File("."));
			int res = inFile.showOpenDialog(ProjectManagerFrame.this);
			if(res == JFileChooser.APPROVE_OPTION)
			{
				String name = inFile.getSelectedFile().getPath();
				try(Scanner inf = new Scanner(Paths.get(name)))
				  {
					    modelPerson.clear();
					    modelTask.clear();
					    modelProject.clear();
					    
					    while (inf.hasNextLine()) {
					      
					    String[] newLine = inf.nextLine().split(" ");
					    String[] opt = newLine[0].split(":");
					    if(opt[0].equals("task"))
					    {
					    	//task:01:time:2018-05-26
					    	if(opt.length < 5)
					    		modelTask.addTask(new Task(opt[1],opt[3]));
					    	//task:02:time:2018-05-26:people:Jan:Kowaslki:m.krzyzak.95@gmail.com
					    	else
					    		modelTask.addTask(new Task(opt[1],opt[3], new Member(opt[5],opt[6],opt[7])));
					    	
					    	modelTask.fireTableDataChanged();
					    }
					    else if(opt[0].equals("person"))
					    {
					    	//person:Jan:Kowaslki:m.krzyzak.95@gmail.com
					    	modelPerson.addPerson(new Member(opt[1],opt[2],opt[3]));
					    	modelPerson.fireTableDataChanged();
					    }
					    else if(opt[0].equals("project"))
					    {
					    	//project:proj1
					    	if(newLine.length < 2)
					    		modelProject.addProject(new Project(opt[1]));

					    	//project:proj1 person:Jan:Kowaslki:m.krzyzak.95@gmail.com task:01:time:2018-05-26
					    	else
					    	{
					            ArrayList<Member> newPersons = new ArrayList<>();
					            ArrayList<Task> newTasks = new ArrayList<>();

					    		for (int i = 1; i < newLine.length; i++) 
					    		{
					    			String[] opt1 = newLine[i].split(":");
									
					    			//people:Jan:Kowaslki:m.krzyzak.95@gmail.com
					    			if(opt1[0].equals("person"))
					    				newPersons.add(new Member(opt1[1],opt1[2],opt1[3]));

					    			//task:01:time:2018-05-26
					    			else if(opt1[0].equals("task"))
					    			{
			        			    	//task:01:time:2018-05-26
			        			    	if(opt1.length < 5)
			        			    		newTasks.add(new Task(opt1[1],opt1[3]));
			        			    	//task:02:time:2018-05-26:people:Jan:Kowaslki:m.krzyzak.95@gmail.com
			        			    	else
			        			    		newTasks.add(new Task(opt[1],opt[3], new Member(opt[5],opt[6],opt[7])));
					    			}
								}
					    		
				    			modelProject.addProject(new Project(opt[1],newTasks,newPersons));
				    			modelProject.fireTableDataChanged();
					    	}
					    }
					    
					    }
				  }
				  catch(IOException e)
				  {
					  JOptionPane.showMessageDialog(ProjectManagerFrame.this, "Input file read failed!", "Import error", JOptionPane.ERROR_MESSAGE);
				  }
			}
		});
		
		
		JMenuItem exportItem = new JMenuItem("Export ->");
		exportItem.addActionListener(event -> {
			outFile.setCurrentDirectory(new File("."));
			int res = outFile.showSaveDialog(ProjectManagerFrame.this);
			if(res == JFileChooser.APPROVE_OPTION)
			{
				String name = outFile.getSelectedFile().getPath();
      		  try(PrintWriter writer = new PrintWriter(name + ".txt"))
      		  {
      			  for (int i = 0; i < modelTask.getRowCount(); i++) 
      				  writer.println(modelTask.getTask(i).getDescriptions());
      			  
      			  for (int i = 0; i < modelPerson.getRowCount(); i++) 
      				  writer.println(modelPerson.getPerson(i).getDescriptions());
      			  
      			  for (int i = 0; i < modelProject.getRowCount(); i++) 
      				  writer.println(modelProject.getProject(i).getDescriptions());
      			  
      			  writer.close();
      		  }
      		  catch(IOException e)
      		  {
      			  JOptionPane.showMessageDialog(ProjectManagerFrame.this, "Output file read failed!", "Export error", JOptionPane.ERROR_MESSAGE);
      		  }
			}
		});
		
		
		impexpMenu.add(importProject);
		impexpMenu.add(exportItem);
		menuBar.add(impexpMenu);
		menuBar.add(impexpMenu);

		setJMenuBar(menuBar);

		
		/*Zawartosc okna*/
		
		JLabel inLabel = new JLabel("\n", JLabel.RIGHT);
		add(inLabel,BorderLayout.NORTH);
		
		tabbedPane = new JTabbedPane();
		tabbedPane.add("People",getPersonTablePanel());
		tabbedPane.add("Task",getTaskTablePanel());
		tabbedPane.add("Project",getProjectTablePanel());
		
		add(tabbedPane);
		
		
    }
	
    private JPanel getPersonTablePanel() 
    {
    	JPanel personPanel = new JPanel(new GridLayout());
    	modelPerson = new PersonTableModel();
		tablePerson = new JTable(modelPerson);
        JScrollPane scroll = new JScrollPane(tablePerson);
        personPanel.add(scroll);
        

        return personPanel;
    }
    
    private JPanel getTaskTablePanel() 
    {
    	JPanel taskPanel = new JPanel(new GridLayout());
		modelTask = new TaskTableModel();
		tableTask = new JTable(modelTask);
        JScrollPane scroll = new JScrollPane(tableTask);
        taskPanel.add(scroll);
        
        return taskPanel;
    }
	
    private JPanel getProjectTablePanel() 
    {
    	JPanel projectPanel = new JPanel(new GridLayout());
		modelProject = new ProjectTableModel();
		tableProject = new JTable(modelProject);
		
        int rowCount = modelProject.getRowCount();
        for (int i = 0; i < rowCount; i++) {
        	JComboBox comboBoxTask = new JComboBox(modelProject.getTask(i).toArray());
        	
		}
        JScrollPane scroll = new JScrollPane(tableProject);
        TableColumn columnTask = tableProject.getColumnModel().getColumn(2); 
        projectPanel.add(scroll);

        return projectPanel;
    }

	/////////////////////// ACTION PERSON ////////////////////////////
	
	private class NewPersonAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			if(addPerson == null) addPerson = new AddPerson();
			
			if(addPerson.showDialog(ProjectManagerFrame.this, "Add person"))
			{
				Member p = addPerson.getPerson();
				modelPerson.addPerson(p);
				modelPerson.fireTableDataChanged();
			}
		}
	}
	
	private class DeletePersonAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			
			int index = tablePerson.getSelectedRow();
			if(index >= 0)
			{
				int ret = JOptionPane.showConfirmDialog(
						ProjectManagerFrame.this, "Are you sure?",
				  "Confirm delete person",
				  JOptionPane.OK_CANCEL_OPTION,
				  JOptionPane.QUESTION_MESSAGE);
				if(ret == JOptionPane.OK_OPTION)
				{
					modelPerson.removePerson(index);
					modelPerson.fireTableDataChanged();
				}

			}
			else JOptionPane.showMessageDialog(null, "Select person");
		}
	}
	
	
	private class ModifyPersonAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			
			int index = tablePerson.getSelectedRow();
			if(index >= 0)
			{
				modifyPerson = new ModifyPerson(modelPerson.getPerson(index));
				
				if(modifyPerson.showDialog(ProjectManagerFrame.this, "Modify person"))
				{
					modelPerson.modifyPerson(index, modifyPerson.getPerson());
					modelPerson.fireTableDataChanged();
				}
			}
			else JOptionPane.showMessageDialog(null, "Select person");
		}
	}
	
	
	/////////////////////// ACTION TASK ////////////////////////////
	
	private class NewTaskAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			if(addTask == null) addTask = new AddTask();
			
			if(addTask.showDialog(ProjectManagerFrame.this, "Add task"))
			{
				Task t = addTask.getTask();
				modelTask.addTask(t);
				modelTask.fireTableDataChanged();
			}
		}
	}
	
	private class DeleteTaskAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			int index = tableTask.getSelectedRow();
			if(index >= 0)
			{
				int ret = JOptionPane.showConfirmDialog(
						ProjectManagerFrame.this, "Are you sure?",
				  "Confirm delete task",
				  JOptionPane.OK_CANCEL_OPTION,
				  JOptionPane.QUESTION_MESSAGE);
				if(ret == JOptionPane.OK_OPTION)
				{
					modelTask.removeTask(index);
					modelTask.fireTableDataChanged();
				}

			}
			else JOptionPane.showMessageDialog(null, "Select task");
				
		}
	}
	
	private class ModifyTaskAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			
			int index = tableTask.getSelectedRow();
			if(index >= 0)
			{
				modifyTask = new ModifyTask(modelTask.getTask(index));
				
				if(modifyTask.showDialog(ProjectManagerFrame.this, "Modify task"))
				{
					modelTask.modifyTask(index, modifyTask.getTask());
					modelTask.fireTableDataChanged();
				}

			}
			else JOptionPane.showMessageDialog(null, "Select task");
		}
	}
	
	
	private class AddPersonTaskAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			int indexPerson = tablePerson.getSelectedRow();
			int indexTask = tableTask.getSelectedRow();
			if(indexPerson >= 0 && indexTask >= 0)
			{
				if(modelTask.personExist(indexTask))
					JOptionPane.showMessageDialog(null, "Executor exist");
				else
				{
					modelTask.addPersonTask(modelPerson.getPerson(indexPerson), indexTask);
					modelTask.fireTableDataChanged();
				}
			}
			else JOptionPane.showMessageDialog(null, "Select person and task");
		}
	}
	
	private class DeletePersonTaskAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			int indexTask = tableTask.getSelectedRow();
			if(indexTask >= 0)
			{
				if(!modelTask.personExist(indexTask))
					JOptionPane.showMessageDialog(null, "Executor not exist");
				else
				{
					modelTask.deletePerson(indexTask);
					modelTask.fireTableDataChanged();
				}
			}
			else JOptionPane.showMessageDialog(null, "Select task");
		}
	}
	
	private class ModifyPersonTaskAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			int indexTask = tableTask.getSelectedRow();
			if(indexTask >= 0)
			{
				if(!modelTask.personExist(indexTask))
					JOptionPane.showMessageDialog(null, "Executor not exist");
				else
				{
					addPerson = new AddPerson();
					
					if(addPerson.showDialog(ProjectManagerFrame.this, "Add person"))
					{
						Member p = addPerson.getPerson();
						modelTask.addPersonTask(p, indexTask);
						modelTask.fireTableDataChanged();
					}
				}
			}
			else JOptionPane.showMessageDialog(null, "Select task");
		}
	}
	
	
	private class SortTaskDescAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			modelTask.sortDesc();
			modelTask.fireTableDataChanged();
		}
	}
	
	private class SortTaskAscAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			modelTask.sortAsc();
			modelTask.fireTableDataChanged();
		}
	}
	
	
	/////////////////////// ACTION PROJECT ////////////////////////////
	
	private class NewProjectAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			addProject = new AddProject();
			
			if(addProject.showDialog(ProjectManagerFrame.this, "Add project"))
			{
				Project p = addProject.getProject();
				modelProject.addProject(p);
				modelProject.fireTableDataChanged();
			}
		}
	}
	
	private class DeleteProjectAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			
			int index = tableProject.getSelectedRow();
			if(index >= 0)
			{
				int ret = JOptionPane.showConfirmDialog(
						ProjectManagerFrame.this, "Are you sure?",
				  "Confirm delete project",
				  JOptionPane.OK_CANCEL_OPTION,
				  JOptionPane.QUESTION_MESSAGE);
				if(ret == JOptionPane.OK_OPTION)
				{
					modelProject.removeProject(index);
					modelProject.fireTableDataChanged();
				}

			}
			else JOptionPane.showMessageDialog(null, "Select project");
		}
	}
	
	
	private class ModifyProjectAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			
			int index = tableProject.getSelectedRow();
			if(index >= 0)
			{
				modifyProject = new ModifyProject(modelProject.getProject(index));
				
				if(modifyProject.showDialog(ProjectManagerFrame.this, "Modify task"))
				{
					modelProject.modifyProject(index, modifyProject.getProject());
					modelProject.fireTableDataChanged();
				}

			}
			else JOptionPane.showMessageDialog(null, "Select project");
		}
	}
	
	private class AddPersonProjectAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			
			int indexProject = tableProject.getSelectedRow();
			int indexPerson = tableProject.getSelectedRow();
			if(indexProject >= 0 && indexProject >= 0)
			{
				modelProject.addPerson(modelPerson.getPerson(indexPerson),indexProject);
				modelProject.fireTableDataChanged();
			}
			else JOptionPane.showMessageDialog(null, "Select person");
		}
	}
	
	
	private class DeletePersonProjectAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			
			int indexProject = tableProject.getSelectedRow();
			if(indexProject >= 0)
			{
				deletePersonProject = new DeletePersonProject(modelProject.getMember(indexProject));
				
				if(deletePersonProject.showDialog(ProjectManagerFrame.this, "Delete person from project"))
				{
					int indexPerson = modifyPersonProject.getIndex();
					
					modelProject.deletePerson(indexProject, indexPerson);
					modelProject.fireTableDataChanged();
				}
			}
			else JOptionPane.showMessageDialog(null, "Select project");
		}
	}
	
	private class ModifyPersonProjectAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			
			int indexProject = tableProject.getSelectedRow();
			if(indexProject >= 0)
			{
				modifyPersonProject = new ModifyPersonProject(modelProject.getMember(indexProject));
				
				if(modifyPersonProject.showDialog(ProjectManagerFrame.this, "Modify person in project"))
				{
					Member m = modifyPersonProject.getPerson();
					int indexPerson = modifyPersonProject.getIndex();
					
					modelProject.modifyPerosn(indexProject, indexPerson, m);
					modelProject.fireTableDataChanged();
				}
			}
			else JOptionPane.showMessageDialog(null, "Select project");
		}
	}
	
	
	private class AddTaskProjectAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			int indexProject = tableProject.getSelectedRow();
			int indexTask = tableTask.getSelectedRow();
			if(indexProject >= 0 && indexTask >= 0)
			{
				modelProject.addTask(modelTask.getTask(indexTask), indexProject);
				modelProject.fireTableDataChanged();
			}
			else JOptionPane.showMessageDialog(null, "Select person");
		}
	}
	
	private class DeleteTaskProjectAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			
			int indexProject = tableProject.getSelectedRow();
			if(indexProject >= 0)
			{
				deleteTaskProject = new DeleteTaskProject(modelProject.getTask(indexProject));
				
				if(deleteTaskProject.showDialog(ProjectManagerFrame.this, "Delete task from project"))
				{
					int indexTask = deleteTaskProject.getIndex();
					
					modelProject.deleteTask(indexProject, indexTask);
					modelProject.fireTableDataChanged();
				}
			}
			else JOptionPane.showMessageDialog(null, "Select project");
		}
	}
	
	private class ModifyTaskProjectAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			
			int indexProject = tableProject.getSelectedRow();
			if(indexProject >= 0)
			{
				modifyTaskProject = new ModifyTaskProject(modelProject.getTask(indexProject));
				
				if(modifyTaskProject.showDialog(ProjectManagerFrame.this, "Modify task in project"))
				{
					Task t = modifyTaskProject.getTask();
					int indexTask = modifyTaskProject.getIndex();
					
					modelProject.modifyTask(indexProject, indexTask, t);
					modelProject.fireTableDataChanged();
				}
			}
			else JOptionPane.showMessageDialog(null, "Select project");
		}
	}
	
	private class AddPersonTaskProjectAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			int indexProject = tableProject.getSelectedRow();
		
			if(indexProject >= 0)
			{
				addPersonTaskProject = new AddPersonTaskProject(modelProject.getTask(indexProject), modelProject.getMember(indexProject));;
				
				if(addPersonTaskProject.showDialog(ProjectManagerFrame.this, "Add person to task in project"))
				{
					int indexTask = addPersonTaskProject.getIndexTask();
					int indexPerson = addPersonTaskProject.getIndexPerson();
					modelProject.addPersonTask(indexProject, indexTask, indexPerson);
					modelProject.fireTableDataChanged();
				}
			}
			else JOptionPane.showMessageDialog(null, "Select project");
		}
	}
	
	private class DeletePersonTaskProjectAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			int indexProject = tableProject.getSelectedRow();
		
			if(indexProject >= 0)
			{
				deletePersonTaskProject = new DeletePeronTaskPeroject(modelProject.getTask(indexProject));
			
				if(deletePersonTaskProject.showDialog(ProjectManagerFrame.this, "Delete person from task in project"))
				{
					int indexTask = addPersonTaskProject.getIndexTask();
					modelProject.deletePersonTask(indexProject, indexTask);
					modelProject.fireTableDataChanged();
				}
			}
			else JOptionPane.showMessageDialog(null, "Select project");
		}
	}
	
	
	private class SortTaskProjectAscAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			int indexProject = tableProject.getSelectedRow();
			if(indexProject >= 0)
			{
				modelProject.sortTaskAsc(indexProject);
				modelProject.fireTableDataChanged();
			}
			else JOptionPane.showMessageDialog(null, "Select project");
		}
	}
	
	private class SortTaskProjectDescAction implements ActionListener 
	{
		public void actionPerformed(ActionEvent event) 
		{
			int indexProject = tableProject.getSelectedRow();
			if(indexProject >= 0)
			{
				modelProject.sortTaskDesc(indexProject);
				modelProject.fireTableDataChanged();
			}
			else JOptionPane.showMessageDialog(null, "Select project");
		}
	}
		
}


	/////////////////////// TABLE MODELS ////////////////////////////

class PersonTableModel extends AbstractTableModel {

	private ArrayList<Member> people = new ArrayList<>();
	private static int columns = 3;
	private String[] columnNames = {"First name", "Last name", "Email"};
	
	public PersonTableModel() {
	}
	
	public int getRowCount() {
		return people.size();
	}
	
	public int getColumnCount() {
		return columns;
	}
	
	public Object getValueAt(int r, int c) {
		return people.get(r).attr(c);
	}
	
	public String getColumnName(int c) {
		return columnNames[c];
	}
	
	public void addPerson(Member p) {
		people.add(p);
	}
	
	public void removePerson(int index) {
		people.remove(index);
	}
	
	public Member getPerson(int index)
	{
		return people.get(index);
	}
	
	public void modifyPerson(int index, Member m)
	{
		people.get(index).modification(m.getFirstName(), m.getLastName(), m.getEmail());
	}
	
	public ArrayList<Member> getPersons() {
		return people;
	}
	
	public void clear()
	{
		people.clear();
	}
}




class TaskTableModel extends AbstractTableModel {

	private ArrayList<Task> task = new ArrayList<>();
	private static int columns = 6;
	private String[] columnNames = {"Title", "Date", "Delay", "E.first name", "E.last name","E.email"};
	
	public TaskTableModel() {
	}
	
	public int getRowCount() {
		return task.size();
	}
	
	public int getColumnCount() {
		return columns;
	}
	
	public Object getValueAt(int r, int c) {
		return task.get(r).attr(c);
	}
	
	public String getColumnName(int c) {
		return columnNames[c];
	}
	
	public void addTask(Task p) {
		task.add(p);
	}
	
	public void removeTask(int index) {
		task.remove(index);
	}
	
	public Task getTask(int index)
	{
		return task.get(index);
	}
	
	public void modifyTask(int index, Task t)
	{
		task.get(index).modification(t.getTitle(),t.getDue().toString());
	}
	
	public ArrayList<Task> getTask() {
		return task;
	}
	
	public void clear()
	{
		task.clear();
	}
	
	public void addPersonTask(Member m, int index)
	{
		task.get(index).assigTo(m);
	}
	
	public boolean personExist(int index)
	{
		if(task.get(index).getExecutor() == null) return false;
		else return true;
	}
	
	public void deletePerson(int index)
	{
		task.get(index).deleteExecutor();
	}
	
	public void sortDesc()
	{
		Collections.sort(task, Task.COMPARE_DESC);
	}
	
	public void sortAsc()
	{
		Collections.sort(task, Task.COMPARE_ASC);
	}
}

class ProjectTableModel extends AbstractTableModel {

	private ArrayList<Project> project = new ArrayList<>();
	private static int columns = 3;
	private String[] columnNames = {"Title", "Task", "People"};
	
	public ProjectTableModel() {
	}
	
	public int getRowCount() {
		return project.size();
	}
	
	public int getColumnCount() {
		return columns;
	}
	
	public Object getValueAt(int r, int c) {
		return project.get(r).attr(c);
	}
	
	public String getColumnName(int c) {
		return columnNames[c];
	}
	
	public void addProject(Project p) {
		project.add(p);
	}
	
	public void removeProject(int index) {
		project.remove(index);
	}
	
	public Project getProject(int index)
	{
		return project.get(index);
	}
	
	public ArrayList<Member> getMember(int index)
	{
		return project.get(index).getMembers();
	}
	
	
	public ArrayList<Task> getTask(int index)
	{
		return project.get(index).getTasks();
	}
	
	
	public void modifyProject(int index, Project t)
	{
		project.get(index).modification(t.getTitle());
	}
	
	
	public ArrayList<Project> getTask() {
		return project;
	}
	
	public void clear()
	{
		project.clear();
	}
	
	public void addPerson(Member m, int index)
	{
		project.get(index).addMember(m);
	}
	
	public void deletePerson(int indexProject, int indexPerson)
	{
		project.get(indexProject).removeMember(indexPerson);
	}
	
	public void modifyPerosn(int indexProject, int indexPerson, Member m)
	{
		project.get(indexProject).modificationMember(indexPerson, m);
	}
	
	public void addTask(Task t, int index)
	{
		project.get(index).addTask(t);
	}
	
	public void deleteTask(int indexProject, int indexTask)
	{
		project.get(indexProject).removeTask(indexTask);
	}
	
	public void modifyTask(int indexProject, int indexTask, Task t)
	{
		project.get(indexProject).modificationTask(indexTask, t);
	}
	
	public void addPersonTask(int indexProject, int indexTask, int indexPerson)
	{
		project.get(indexProject).addPersonTask(indexTask, project.get(indexProject).getMembers().get(indexPerson));
	}
	
	public void deletePersonTask(int indexProject, int indexTask)
	{
		project.get(indexProject).deletePerosnTask(indexTask);
	}
	
	public void sortTaskDesc(int index)
	{
		project.get(index).SortTaskDesc();
	}
	
	public void sortTaskAsc(int index)
	{
		project.get(index).SortTaskAsc();
	}
	
}


