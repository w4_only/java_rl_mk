import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import apps.people.Member;
import apps.projects.Task;

public class AddTask extends JPanel
{

	private JTextField title;
    private JTextField date;
    
    private JButton addButton;
    private JButton cancelButton;
    private boolean ok;
    private JDialog dialog;
    
    public AddTask() {
    	setLayout(new BorderLayout());
    	JPanel panel = new JPanel();
    	panel.setLayout(new GridLayout(2, 2));
    	panel.add(new JLabel("Title:"));
    	panel.add(title = new JTextField(""));
    	panel.add(new JLabel("Date:"));
    	panel.add(date = new JTextField(""));
    	add(panel, BorderLayout.CENTER);
    	
    	addButton = new JButton("Add");
    	addButton.addActionListener(event -> {
    		ok = true;
    		dialog.setVisible(false);
    	});
    	
    	cancelButton = new JButton("Cancel");
    	cancelButton.addActionListener(event -> dialog.setVisible(false));
    	
    	JPanel buttonPanel = new JPanel();
    	buttonPanel.add(addButton);
    	buttonPanel.add(cancelButton);
    	add(buttonPanel, BorderLayout.SOUTH);
    }
    
    public Task getTask() {
    	return new Task(title.getText(),
    					date.getText()
    					);
    }
    
    public boolean showDialog(Component parent, String title) {
    	ok = false;
    	
    	Frame owner = null;
    	if(parent instanceof Frame)
    		owner = (Frame) parent;
    	else
    		owner = (Frame) SwingUtilities.getAncestorOfClass(Frame.class, parent);
    	
    	if(dialog == null || dialog.getOwner() != owner)
    	{
    		dialog = new JDialog(owner, true);
    		dialog.add(this);
    		dialog.getRootPane().setDefaultButton(addButton);
    		dialog.pack();
    	}
    	
    	dialog.setTitle(title);
    	dialog.setVisible(true);
    	return ok;
    }

}
