import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import apps.projects.Project;


public class AddProject extends JPanel
{
	private JTextField title;
	private JTextField person;
	private JTextField task;

    private JButton addButton;
    private JButton cancelButton;
    private boolean ok;
    private JDialog dialog;
    
    public AddProject() {
    	setLayout(new BorderLayout());
    	JPanel panel = new JPanel();
    	panel.setLayout(new GridLayout(1, 2));
    	panel.add(new JLabel("Title:"));
    	panel.add(title = new JTextField(""));

    	add(panel, BorderLayout.CENTER);
    	
    	addButton = new JButton("Add");
    	addButton.addActionListener(event -> {
    		ok = true;
    		dialog.setVisible(false);
    	});
    	
    	cancelButton = new JButton("Cancel");
    	cancelButton.addActionListener(event -> dialog.setVisible(false));
    	
    	JPanel buttonPanel = new JPanel();
    	buttonPanel.add(addButton);
    	buttonPanel.add(cancelButton);
    	add(buttonPanel, BorderLayout.SOUTH);
    }
    
    public Project getProject() {
    	return new Project(title.getText()
    					);
    }
    
    public boolean showDialog(Component parent, String title) {
    	ok = false;
    	
    	Frame owner = null;
    	if(parent instanceof Frame)
    		owner = (Frame) parent;
    	else
    		owner = (Frame) SwingUtilities.getAncestorOfClass(Frame.class, parent);
    	
    	if(dialog == null || dialog.getOwner() != owner)
    	{
    		dialog = new JDialog(owner, true);
    		dialog.add(this);
    		dialog.getRootPane().setDefaultButton(addButton);
    		dialog.pack();
    	}
    	
    	dialog.setTitle(title);
    	dialog.setVisible(true);
    	return ok;
    }

}
