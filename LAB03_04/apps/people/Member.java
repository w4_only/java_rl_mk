package apps.people;

public class Member
{
  private static int nextId = 1;
  private int id;
  private String firstName;
  private String lastName;
  private String email;

  public Member(String fn, String ln, String e)
  {
    firstName = fn;
    lastName = ln;
    email = e;
    id = nextId++; 
  }

  public String toString()
  {
    return getDescriptions();
  }
  
  /* getery */
  
  public int getIndex()
  {
	  return id;
  }
  
  public String getFirstName()
  {
    return firstName;
  }

  public String getLastName()
  {
    return lastName;
  }
  
  public String getEmail()
  {
    return email;
  }
  
  public String getId()
  {
    return Integer.toString(id);
  }
  
  public void modification(String fn, String ln, String e)
  {
	    firstName = fn;
	    lastName = ln;
	    email = e;
  }
  
  public String getDescriptions() {
	  return "person:" + firstName + ":" + lastName + ":" + email + "";
  }
  
  
  interface GetAttr {
	  String attr();
  }
  
  private GetAttr[] attrs = new GetAttr[] {	  
    new GetAttr() { public String attr() { return getFirstName();} },
    new GetAttr() { public String attr() { return getLastName();} },
    new GetAttr() { public String attr() { return getEmail();} },
  };

  public String attr(int index)
  {
	return attrs[index].attr();  
  }
  
}
