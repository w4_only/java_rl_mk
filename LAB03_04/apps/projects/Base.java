package apps.projects;

abstract public  class  Base
{
  private String title;
  private static int nextId = 1;
  private int id;
  public int index;
  
  public Base(String t)
  {
    title = t;
    id = nextId++;
    index = id;
  }
  
  public String getTitle()
  {
	  return title;
 }
  
 public int getId()
  {
	  return id;
  }
  
  public void modification(String t)
  {
	  title = t;
  }
  
  public abstract int getIndex();
  
  public abstract String toString();
  
  public abstract  boolean equals(Object other);
  
 /* public String toString()
  {
	  return title;
  }*/
}
