package apps.projects;
import java.time.*;
import java.util.Comparator;

import apps.people.Member;

public class Task 
extends Base
implements Comparable<Task>
{
	private Member executor;
	private LocalDate due;
	private Integer Delay;
	
  public Task(String t)
	  {
	    super(t);
	    due=LocalDate.now().plusDays(10);
	    Delay = getDelay();
	  }

  public Task(String t, String  d)
  {
    super(t);
    due=LocalDate.parse(d);
    Delay = getDelay();
  }

  public Task(String t, String d, Member e)
  {
    super(t);
    due=LocalDate.parse(d);
    executor = e;
    Delay = getDelay();
  }
  
  public Task(String t, String d, String f, String l, String e)
  {
    super(t);
    due=LocalDate.parse(d);
    executor = new Member(f,l,e);
    Delay = getDelay();
  }

  public void assigTo(Member to)
  {
    executor = to;
  }
  
  public void deleteExecutor()
  {
	  executor = null;
  }

  public Member getExecutor()
  {
    return executor;
  }
  
  public String toString()
  {
	  return getDescriptions();
  }
  
  public boolean delayed() {
	  if(getDelay()<0)
		  return true;
	  return false;
  }
  

  public String getDescriptions() {
	  if(executor!=null)
		  return "task:" + getTitle() + ":time:" + getDue() + ":people:" + getExecutor();

	  else
		  return "task:" + getTitle()  + ":time:" + getDue();
  }

  public int compareTo(Task other) 
  {
	  return Delay.compareTo(other.Delay);
  }
  
  
@Override
public boolean equals(Object other) {
	if (other == null) return false;
    if (other == this) return true;
    if (!(other instanceof Task))return false;
    Task otherTask = (Task)other;
	return false;
}

public int getIndex() 
{
	return index;
}

public String getExecutorFirstName()
{
	if(executor!=null)
		return executor.getFirstName();
	else
		return null;
}

public String getExecutorLastName()
{
	if(executor!=null)
		return executor.getLastName();
	else
		return null;
}

public String getExecutorEmail()
{
	if(executor!=null)
		return executor.getEmail();
	else
		return null;
}

public LocalDate getDue() {
	  return due;
}

public int getDelay() 
{
	  return LocalDate.now().until(due).getDays();
}


interface GetAttr {
	  String attr();
}

private GetAttr[] attrs = new GetAttr[] {
  new GetAttr() { public String attr() { return getTitle();} },
  new GetAttr() { public String attr() { return getDue().toString();} },
  new GetAttr() { public String attr() { return Integer.toString(getDelay());} },
  new GetAttr() { public String attr() { return getExecutorFirstName(); } },
  new GetAttr() { public String attr() { return getExecutorLastName();} },
  new GetAttr() { public String attr() { return getExecutorEmail();} },
};

public String attr(int index)
{
	return attrs[index].attr();  
}

public void modification(String t, String d)
{
	modification(t);
	due=LocalDate.parse(d);
	Delay = getDelay();
}

public static Comparator<Task> COMPARE_ASC = new Comparator<Task>() {
    public int compare(Task one, Task other) {
        return one.compareTo(other);
    }
};

public static Comparator<Task> COMPARE_DESC = new Comparator<Task>() {
    public int compare(Task one, Task other) {
        return other.compareTo(one);
    }
};

}

