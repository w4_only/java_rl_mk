package apps.projects;

import apps.people.Member;
import apps.projects.Task.GetAttr;

import java.util.*;

public class Project extends Base
{
  private ArrayList<Task> tasks;
  private ArrayList<Member> members;

  public Project(String t)
  {
    super(t);
    tasks = new ArrayList<>();
    members = new ArrayList<>();
  }
  
  public Project(String t, ArrayList<Task> newTask, ArrayList<Member> newMembers)
  {
    super(t);
    tasks = newTask;
    members = newMembers;
  }

  public void addTask(Task t)
  {
    tasks.add(t);
  }

  public void addMember(Member m)
  {
    members.add(m);
  }
  public void removeTask(Task t)
  {
    tasks.remove(t);
  }
  
  public void modificationTask(int index, Task t)
  {
	  tasks.set(index,t);
  }
  
  public void modificationMember(int index, Member m)
  {
	  members.set(index,m);
  }
  
  public void removeTask(int index)
  {
	  tasks.remove(index);
  }
  
  public void removeMember(int index)
  {
	  members.remove(index);
  }
  
  public ArrayList<Member> getMembers()
  {
	  return members;
  }
  
  public ArrayList<Task> getTasks()
  {
	  return tasks;
  }
  
  public void addPersonTask(int indexTask, Member m)
  {
	  tasks.get(indexTask).assigTo(m);
  }
  
  public void deletePerosnTask(int indexTask)
  {
	  tasks.get(indexTask).deleteExecutor();
  }
  
  public void showTask()
  {
	  for (int i = 0; i < tasks.size(); i++) 
		System.out.println(i + ") " + tasks.get(i).toString());
  }
  public void showMember()
  {
	  for (int i = 0; i < members.size(); i++) 
		System.out.println(i + ") " + members.get(i).toString());
  }
  
  public String toString()
  {
    return getTitle();
  }
 
  public String getDescriptions() {
	  String description = "";

	  description += "project:" + toString() + " ";
	  for (int i = 0; i < members.size(); i++)
		  description += members.get(i).getDescriptions() + " ";
	  
	  for (int i = 0; i < tasks.size(); i++)
		  description += tasks.get(i).getDescriptions() + " ";

	  return description;
  }
  public int getIndex() {
		return index;
	}

@Override
public boolean equals(Object other) {
	if (other == null) return false;
    if (other == this) return true;
    if (!(other instanceof Task))return false;
    Task otherTask = (Task)other;
	return false;
	
}

public String getDestTask()
{
	String desc = "";
	for (int i = 0; i < tasks.size(); i++) 
	{
		desc = desc + tasks.get(i).getDescriptions() + "  ";
	}
	return desc;
}

public String getDestPeople()
{
	String desc = "";
	for (int i = 0; i < members.size(); i++) 
	{
		desc = desc + members.get(i).getDescriptions() + "  ";
	}
	return desc;
}


interface GetAttr {
	  String attr();
}

private GetAttr[] attrs = new GetAttr[] {
new GetAttr() { public String attr() { return getTitle();} },
new GetAttr() { public String attr() { return getDestTask();} },
new GetAttr() { public String attr() { return getDestPeople();} }
};

public String attr(int index)
{
	return attrs[index].attr();  
}

public void SortTaskAsc()
{
	Collections.sort(tasks, Task.COMPARE_ASC);
}

public void SortTaskDesc()
{
	Collections.sort(tasks, Task.COMPARE_DESC);
}

}
