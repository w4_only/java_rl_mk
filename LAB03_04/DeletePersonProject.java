import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import apps.people.Member;



public class DeletePersonProject extends JPanel
{
    private JButton addButton;
    private JButton cancelButton;
    private boolean ok;
    private JDialog dialog;
    private int selectedIndex;
    
    public DeletePersonProject(ArrayList<Member> members) {
    	setLayout(new BorderLayout());
    	JPanel panel = new JPanel();
    	panel.setLayout(new GridLayout(1, 2));
    	panel.add(new JLabel("Select person:"));
    	JComboBox comboBox = new JComboBox(members.toArray());
    	panel.add(comboBox);
    	selectedIndex = comboBox.getSelectedIndex();
    	add(panel, BorderLayout.CENTER);
    	
    	addButton = new JButton("Delete");
    	addButton.addActionListener(event -> {
    		ok = true;
    		dialog.setVisible(false);
    	});
    	
    	cancelButton = new JButton("Cancel");
    	cancelButton.addActionListener(event -> dialog.setVisible(false));
    	
    	JPanel buttonPanel = new JPanel();
    	buttonPanel.add(addButton);
    	buttonPanel.add(cancelButton);
    	add(buttonPanel, BorderLayout.SOUTH);
    }
    
    
    
	public int getIndex()
	{
		return selectedIndex;
	}
    
    public boolean showDialog(Component parent, String title) {
    	ok = false;
    	
    	Frame owner = null;
    	if(parent instanceof Frame)
    		owner = (Frame) parent;
    	else
    		owner = (Frame) SwingUtilities.getAncestorOfClass(Frame.class, parent);
    	
    	if(dialog == null || dialog.getOwner() != owner)
    	{
    		dialog = new JDialog(owner, true);
    		dialog.add(this);
    		dialog.getRootPane().setDefaultButton(addButton);
    		dialog.pack();
    	}
    	
    	dialog.setTitle(title);
    	dialog.setVisible(true);
    	return ok;
    }
	

}
