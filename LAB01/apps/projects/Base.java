package apps.projects;

class Base
{
  private String title;

  public Base(String t)
  {
    title = t;
  }

  public String getTitle()
  {
    return title;
  }
  
  public void modification(String t)
  {
	  title = t;
  }
}
