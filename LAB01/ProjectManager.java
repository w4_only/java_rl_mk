import apps.projects.Task;
import apps.projects.Project;
import apps.people.Member;
import java.util.*;

class ProjectManger
{
	
	
    public static void main(String[] args)
    {
    	Scanner ins = new Scanner(System.in);
    	
        ArrayList<Member> person = new ArrayList<>();
        ArrayList<Task> task = new ArrayList<>();
        ArrayList<Project> project = new ArrayList<>();

        while(true)
        {
          System.out.print("> ");
          
          String userInput = ins.nextLine();
          String[] cmd= userInput.split(" ");

          if(cmd[0].equals("quit"))
            break;
          else
          {
        	  /******************************************DODAWANIE****************************************/
        	  if(cmd[0].equals("add"))
        	  {
        		  //add project P01
        		  if(cmd[1].equals("project") && !(cmd[2].isEmpty()))
        			  project.add(new Project(cmd[2]));
        		  
        		  //add person:0 project:0
        		  else if(cmd[1].contains("person:") && cmd[2].contains("project:"))
        		  {
            			  String[] opt1, opt2 = {"",""};
            			  
            			  opt1 = cmd[1].split(":"); //opcje dotyczace osoby
            			  opt2 = cmd[2].split(":"); //opcje dotyczace projektu
            			  
        				  //pobiertanie zadanie, ktore ma zostac dodane
        				  Member newMember = person.get(Integer.parseInt(opt1[1]));
        				  //pobieranie projektu, do ktorego ma by przypisane zadanie
        				  Project newProject = project.get(Integer.parseInt(opt2[1]));
        				  //dodawanie nowego zadania
        				  newProject.addMember(newMember);
        				  //zapisywanie zmian do listy projekt�w
        				  project.set(Integer.parseInt(opt2[1]), newProject);
        		  }
        		  //add person Jan Kowalski email@gmail.com
        		  else if(cmd[1].equals("person") && !(cmd[2].isEmpty()) && !(cmd[3].isEmpty()) && !(cmd[4].isEmpty()))
        			  person.add(new Member(cmd[2],cmd[3],cmd[4]));

        		  //add task:1 project:1
        		  else if(!(cmd[1].isEmpty()) && !(cmd[2].isEmpty()))
        		  {
        			  String[] opt1, opt2 = {"",""};
        			  
        			  opt1 = cmd[1].split(":"); //opcje dotyczace zadania
        			  opt2 = cmd[2].split(":"); //opcje dotyczace projektu
        			  if(opt1[0].equals("task") && !(opt1[1].isEmpty()) && opt2[0].equals("project") && !(opt2[1].isEmpty()))
        			  {
        				  //pobiertanie zadanie, ktore ma zostac dodane
        				  Task newTask = task.get(Integer.parseInt(opt1[1]));
        				  //pobieranie projektu, do ktorego ma by przypisane zadanie
        				  Project newProject = project.get(Integer.parseInt(opt2[1]));
        				  //dodawanie nowego zadania
        				  newProject.addTask(newTask);
        				  //zapisywanie zmian do listy projekt�w
        				  project.set(Integer.parseInt(opt2[1]), newProject);
        			  }
        			  else System.out.println("Wrong command");
        		  }
        		  //add task T01
        		  else if(cmd[1].equals("task") && !(cmd[2].isEmpty()))
        			  task.add(new Task(cmd[2]));
        		  
        		  else System.out.println("Wrong command");
        		  
        	  }
        	  
        	  /**********************************************MODYFIKACJA*********************************************/
        	  else if(cmd[0].equals("modify"))
        	  {
    			  String[] opt1, opt2 = {"",""};
    			  
        		  //parsowanie opcji, informujacej, ktory element ma byc zmodyfikowany
        		  opt1 = cmd[1].split(":");
    			  opt2 = cmd[2].split(":");

        		  //modify project:1 P34
        		  if(cmd[1].contains("project:") && !(opt1[1].isEmpty()) && !(cmd[2].isEmpty()))
        		  {
        			  Project newProject = project.get(Integer.parseInt(opt1[1]));
        			  newProject.modification(cmd[2]);
        			  project.set(Integer.parseInt(opt1[1]), newProject);
        		  }
        		  
        		  //modify task:1 project:1 P76
        		  else if(cmd[1].contains("task:") && !(opt1[1].isEmpty()) && cmd[2].contains("project:") && !(opt2[1].isEmpty()) && !(cmd[3].isEmpty()))
        		  {
        			  Task newTask = new Task(cmd[3]);
        			  Project newProject = project.get(Integer.parseInt(opt2[1]));
        			  newProject.modificationTask(Integer.parseInt(opt1[1]), newTask);
        			  project.set(Integer.parseInt(opt2[1]), newProject);
        		  }
        		  //modify task:1 T34
        		  else if(cmd[1].contains("task:") && !(opt1[1].isEmpty()) && !(opt2[0].isEmpty()))
        		  {
        			  Task newTask = new Task(opt2[0]);
        			  task.set(Integer.parseInt(opt1[1]), newTask);
        		  }
        		  
        		  //modify person:1 project:1 Jan Kowaski gmail@email.com
        		  else if(cmd[1].contains("person:") && !(opt1[1].isEmpty()) && cmd[2].contains("project:") && !(opt2[1].isEmpty()) && !(cmd[3].isEmpty()) && !(cmd[4].isEmpty())&& !(cmd[5].isEmpty()))   
        		  {
        			  Member newMember = new Member(cmd[3],cmd[4],cmd[5]);
        			  Project newProject = project.get(Integer.parseInt(opt2[1]));
        			  newProject.modificationMember(Integer.parseInt(opt1[1]), newMember);
        			  project.set(Integer.parseInt(opt2[1]), newProject);
        		  }
        		  
        		  //modify person:1 Jan Kowaski gmail@email.com
        		  else if(cmd[1].contains("person:") && !(cmd[2].isEmpty()) && !(cmd[3].isEmpty()) && !(cmd[4].isEmpty()))
        		  {
        			  Member newMember = new Member(opt2[0],cmd[3],cmd[4]);
        			  person.set(Integer.parseInt(opt1[1]), newMember);
        		  }
        		  
        		  else System.out.println("Wrong command");
        	  }
        	  /*****************************************************USUWANIE*******************************************************/
        	  else if(cmd[0].equals("delete"))
        	  {
    			  String[] opt1, opt2 = {null,null};
    			  
        		  // delete project:1
        		  if(cmd[1].contains("project:"))
        		  {
        			  opt1 = cmd[1].split(":");
        			  project.remove(opt1[1]);
        		  }
        			          		  
        		  //delete person:1
        		  else if(cmd[1].contains("person:"))
        		  {
        			  opt1 = cmd[1].split(":");
        			  person.remove(opt1[1]);
        		  }

        		  //delete task:1 project:1
        		  else if(cmd[1].contains("task:") && cmd[2].contains("project:"))
        		  {
        			  opt1 = cmd[1].split(":");
        			  task.remove(opt1[1]);
        		  }
        			  

        		  else System.out.println("Wrong command");
 
        	  }
        	  
        	  /***********************************************WSWIETLANIE************************************************/
        	  else if(cmd[0].equals("list"))
        	  {
        		  
        		  if(cmd[1].equals("project"))
        			  for(int i=0; i < project.size(); i++)
        				  System.out.println(i + ") " + project.get(i).toString());
        		  
        		  else if(cmd[1].equals("task") && cmd[2].contains("project:"))
        		  {
        			  String[] opt2 = cmd[2].split(":");
        			  Project newProject = project.get(Integer.parseInt(opt2[1]));
        			  newProject.showTask();
        		  }
        		  
        		  else if(cmd[1].equals("task"))
        			  for(int i=0; i < task.size(); i++)
        				  System.out.println(i + ") " + task.get(i).toString());
        		  
        		  else if(cmd[1].equals("person") && cmd[2].equals(""))
        			  for(int i=0; i < person.size(); i++)
        				  System.out.println(i + ") " + person.get(i).toString());
        		  //list person project:0
        		  else if(cmd[1].equals("person") && cmd[2].contains("project:"))
        		  {
        			  String[] opt2 = cmd[2].split(":");
        			  Project newProject = project.get(Integer.parseInt(opt2[1]));
        			  newProject.showMember();
        		  }
        		  //list person

 
        		  else System.out.println("Wrong command");
        		  
        	  }
        	  else System.out.println("Wrong commmmmmand");
          }
        }
    }
}
