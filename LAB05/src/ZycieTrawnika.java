import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

public class ZycieTrawnika {
	
	public static void main(String[] args) throws IOException, InterruptedException {
		
		int iloscSlimakow = 0;
		int rozmiarTrawnika = 0;
		int iloscTrawy = 0;
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Ilosc slimakow: ");
		if(in.hasNextInt())
		{
			iloscSlimakow = in.nextInt();
		}
		else
			System.out.println("Bledne dane!");
		
		System.out.println("Rozmiar trawnika: ");
		if(in.hasNextInt())
		{
			rozmiarTrawnika = in.nextInt();
		}
		else
			System.out.println("Bledne dane!");
		
		System.out.println("Ilosc trawy: ");
		if(in.hasNextInt())
		{
iloscTrawy = in.nextInt();
		}
		else
			System.out.println("Bledne dane!");
			

		
		Trawnik trawnik = new Trawnik(rozmiarTrawnika,iloscTrawy);
		
	    Thread watekTrawnik = new Thread(new WatekTrawa(trawnik));
	    watekTrawnik.start();
		
		Thread[] watekSlimak = new Thread[iloscSlimakow];
		
		for (int i = 0; i < iloscSlimakow; i++) {
			watekSlimak[i] = new Thread(new WatekSlimak(trawnik,i + 1));
			watekSlimak[i].start();
		}
		
		while(trawnik.iloscTrawy() >= 0)
		{
			if(trawnik.iloscTrawy() >0)
			{
			trawnik.wyswietl();
			System.out.println();
			Thread.sleep(1000);
			}
			else 
			{
				trawnik.wyswietl();
				System.out.println("Kurła zeżarły wszystko te szkodniki");
				break;
			}
		}
		
	}

}
