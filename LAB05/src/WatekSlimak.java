
public class WatekSlimak implements Runnable {
	
	private Trawnik trawnik;
	private Slimak slimak;
	
	public WatekSlimak(Trawnik trawnik, int numer)
	{
		this.trawnik = trawnik;
		slimak = new Slimak(trawnik, numer);
	}
	
	@Override
	public void run() {
		//System.out.println("Teraz je slimak " + slimak.numer());
	    try
	    {	
	    	while(true)
	    	{
	    		slimak.jedz(trawnik);
	    		Thread.sleep(10);
	    	}
	     
	    }
	    catch (Exception e)
	    {
	       System.out.println("Przerwano");
	    }
		
	}
}

