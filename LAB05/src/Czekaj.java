public class Czekaj {
	
	public static synchronized void manySec(long s) 
	{
		try 
		{
			Thread.currentThread().sleep(s * 10);
		}
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
	}
}