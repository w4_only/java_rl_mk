
public class Pole {
	
	private int iloscTrawy;
	public Pole(int iloscTrawy) {
		this.iloscTrawy = iloscTrawy;
	}
	
	public int iloscTrawy()
	{
		return iloscTrawy;
	}
	
	public synchronized void  zmniejsz(int ilosc)
	{
		int nowaIlosc = iloscTrawy - ilosc;
		if(nowaIlosc < 0)
			iloscTrawy = 0;
		else 
			iloscTrawy = nowaIlosc;
	}
	
	public synchronized void  zwieksz(int ilosc, int zakres)
	{
		int nowaIlosc = iloscTrawy + ilosc;
		if(nowaIlosc > zakres)
			iloscTrawy = zakres;
		else 
			iloscTrawy = nowaIlosc;
	}
}
