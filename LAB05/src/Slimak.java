import java.util.Random;

public class Slimak{
	
	private Trawnik trawnik;
	private int numer;
	
	public Slimak(Trawnik trawnik, int numer)
	{
		this.trawnik = trawnik;
		this.numer = numer;
	}
	
	public void jedz(Trawnik trawnik)
	{
		Random rand = new Random();
		int i = rand.nextInt(trawnik.rozmiar());
		int j = rand.nextInt(trawnik.rozmiar());
		
		trawnik.zmniejszTrawe(i, j);
	}
	
	public int numer()
	{
		return numer;
	}
}
