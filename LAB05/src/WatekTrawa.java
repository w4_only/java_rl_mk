
public class WatekTrawa implements Runnable {
	
	private Trawnik trawnik;
	
	public WatekTrawa(Trawnik trawnik)
	{
		this.trawnik = trawnik;
	}
	
	@Override
	public void run() {
		//System.out.println("Teraz teraz rosnie trawa");
	    try
	    {
	    	while(true)
	    	{
	    		trawnik.zwiekszTrawe();
	    		Thread.sleep(10);
	    	}
	      	      
	    }
	    catch (Exception e)
	    {
	       System.out.println("Przerwano");
	    }
		
	}
}

