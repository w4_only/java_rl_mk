import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Trawnik {
	
	private Pole [][] pole;
	private int rozmiar;
	private int zakres;
	
	Trawnik(int rozmiar, int zakres)
	{
		this.rozmiar = rozmiar;
		this.zakres = zakres;
		pole = new Pole[rozmiar][rozmiar];
		Random rand = new Random();
		for(int j=0; j<rozmiar; j++)
		{
			for(int k=0; k<rozmiar; k++)
			{
				pole[j][k] = new Pole(rand.nextInt(zakres + 1));
			}				
		}
	}
	
	public boolean zmniejszTrawe(int i, int j)
	{
		if(i<0 || j<0)
			return false;
		
		if(pole[i][j].iloscTrawy() > 0)
		{
			pole[i][j].zmniejsz(4);
			return true;
		}
		else return true;
	}
	
	public void zwiekszTrawe() 
	{
		Random rand = new Random();
		for(int j=0; j<rozmiar; j++)
		{
			for(int k=0; k<rozmiar; k++)
			{
				if(pole[j][k].iloscTrawy() < zakres)
				{
					int wzrost = rand.nextInt(zakres + 1);
					pole[j][k].zwieksz(1, zakres);
				}
					
			}				
		}
	}
	
	public  void wyswietl()
	{
		for(int j=0; j<pole.length; j++)
		{
			for(int k=0; k<pole.length; k++)
			{
				System.out.print("[");
				for(int i = 0; i<zakres; i++)
				{
					if(i < pole[j][k].iloscTrawy())
						System.out.print("^");
					else
						System.out.print(" ");
				}
				
				System.out.print("]");
			}
			System.out.print("\n");
		}		
	}
	
	public int iloscTrawy()
	{
		int ilosc = 0;
		for(int j=0; j<rozmiar; j++)
		{
			for(int k=0; k<rozmiar; k++)
			{
				ilosc += pole[j][k].iloscTrawy();
				//if(ilosc<=0) {
					//wyswietl();
					//System.out.println("zezarte");
				//	break;
				//}
			}				
		}
		return ilosc;
	}
	
	
	public int rozmiar()
	{
		return rozmiar;
	}	
	
	
}
