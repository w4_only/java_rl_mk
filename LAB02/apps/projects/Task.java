package apps.projects;
import java.time.*;
import apps.people.Member;

public class Task extends Base
{
	private Member executor;
	private LocalDate due;
	
  public Task(String t)
	  {
	    super(t);
	    due=LocalDate.now().plusDays(10);
	  }

  public Task(String t, String  d)
  {
    super(t);
    due=LocalDate.parse(d);
  }

  public Task(String t, String d, Member e)
  {
    super(t);
    due=LocalDate.parse(d);
    executor = e;
  }

  public void assigTo(Member to)
  {
    executor = to;
  }

  public Member getExecutor()
  {
    return executor;
  }
  
  public String toString()
  {
	  return getTitle();
  }

  
  public LocalDate getDue() {
	  return due;
  }
  
  public int getDelay() {
	  return LocalDate.now().until(due).getDays();
  }
  
  public boolean delayed() {
	  if(getDelay()<0)
		  return true;
	  return false;
  }
  
  public int compareTo(Task obj) 
  {
	  return LocalDate.now().until(due).getDays();
  }
  
  public String getDescriptions() {
	  if(executor!=null)
		  return "task:" + toString() + ":time:" + getDue() + ":people:" + getExecutor();

	  else
		  return "task:" + toString()  + ":time:" + getDue();
  }

}

