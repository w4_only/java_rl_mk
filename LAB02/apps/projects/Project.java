package apps.projects;

import apps.people.Member;

import java.util.*;

public class Project extends Base
{
  private ArrayList<Task> tasks;
  private ArrayList<Member> members;

  public Project(String t)
  {
    super(t);
    tasks = new ArrayList<>();
    members = new ArrayList<>();
  }
  
  public Project(String t, ArrayList<Task> newTask, ArrayList<Member> newMembers)
  {
    super(t);
    tasks = newTask;
    members = newMembers;
  }

  public void addTask(Task t)
  {
    tasks.add(t);
  }

  public void addMember(Member m)
  {
    members.add(m);
  }
  public void removeTask(Task t)//moja
  {
    tasks.remove(t);
  }
  
  public void modificationTask(int index, Task t)
  {
	  tasks.set(index,t);
  }
  
  public void modificationMember(int index, Member m)
  {
	  members.set(index,m);
  }
  
  public void removeTask(int index)
  {
	  tasks.remove(index);
  }
  
  public void removeMember(int index)
  {
	  members.remove(index);
  }
  
  public void showTask()
  {
	  for (int i = 0; i < tasks.size(); i++) 
		System.out.println(i + ") " + tasks.get(i).toString());
  }
  public void showMember()
  {
	  for (int i = 0; i < members.size(); i++) 
		System.out.println(i + ") " + members.get(i).toString());
  }
  
  public String toString()
  {
    return getTitle();
  }
 
  public String getDescriptions() {
	  String description = "";

	  description += "project:" + toString() + " ";
	  for (int i = 0; i < members.size(); i++)
		  description += members.get(i).getDescriptions() + " ";
	  
	  for (int i = 0; i < tasks.size(); i++)
		  description += tasks.get(i).getDescriptions() + " ";

	  return description;
  }
}
