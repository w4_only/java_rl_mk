package apps.projects;

public abstract class  Base
{
  private String title;
  private static int nextId = 1;
  private int id;

  public Base(String t)
  {
    title = t;
    id = nextId++;
  }
  
  public String getTitle()
  {
	  return title;
 }
  public int getId()
  {
	  return id;
  }

  public void modification(String t)
  {
	  title = t;
  }
  
  public String toString()
  {
	  return title;
  }
}
